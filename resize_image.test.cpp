#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <fstream>
#include <string>
#include <getopt.h>
#include <utility>
#include <vector>
#include <algorithm>

int main(int argc, char *argv[]) {
	char *list_f = NULL;
	struct option longopts[] = {
		{ "list_file", optional_argument, NULL, 'l'},
		{     0,    0,    0,    0},
	};
	int c = -1;
	int remained_arg_idx = 1;
	while((c = getopt_long(argc, argv, ":l:", longopts, NULL)) != -1){
		switch (c){
		case 'l':
			list_f = optarg;
			break;
		}
		remained_arg_idx += 1; 
	}

	int resize_width = -1, resize_height = -1;
	std::vector<std::pair<std::string, std::string> > in_out;
	if (list_f == NULL) {
		if (argc != 5) {
			printf("Usage: \n%s image_in image_out resize_width resize_height\n", __FILE__);
			return 0;
		}
		sscanf(argv[3], "%d", &resize_width);
		sscanf(argv[4], "%d", &resize_height);
		in_out.push_back( std::make_pair<std::string, std::string>(argv[1], argv[2]) );
	} else {
		if (remained_arg_idx + 2 != argc) {
			printf("Usage: \n%s --list_file=<list_file> resize_width resize_height\n", __FILE__);
			printf("%d\n", argc);
			return 0;
		}
		sscanf(argv[remained_arg_idx + 0], "%d", &resize_width);
		sscanf(argv[remained_arg_idx + 1], "%d", &resize_height);

		std::ifstream flist(list_f);
		std::string inf, outf;
		while (flist >> inf >> outf) {
			in_out.push_back( std::make_pair(inf, outf) );
		}
	}

	if (resize_width <= 0 || resize_height <= 0) {
		printf("Invalid resize_width (%d) or resize_height (%d)", resize_width, resize_height);
		return 0;
	}

	printf("Converting %lu images to width (%d) X height (%d)\n", in_out.size(), resize_width, resize_height);
	int tot_converted = 0;
	for (int idx = 0; idx < in_out.size(); ++idx) {
		try {
			printf("converting: %s to %s\n", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			cv::Mat img = cv::imread(in_out[idx].first.c_str());
			cv::Mat out_img;
			cv::resize(img, out_img, cv::Size(resize_width, resize_height));
			cv::imwrite(in_out[idx].second.c_str(), out_img);
			printf("converted: %s to %s\n", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			tot_converted += 1;
		} catch (std::exception e) {
			printf("error: while converting file %s to %s\n", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			e.what();
		}
	}
	printf("Converted %d / %lu images sucessfully\n", tot_converted, in_out.size());

	fclose(stdout);
	fclose(stderr);

	return 0;
}
	
