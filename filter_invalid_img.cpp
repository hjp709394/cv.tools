#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <fstream>
#include <string>
#include <getopt.h>
#include <utility>
#include <vector>
#include <algorithm>

int main(int argc, char *argv[]) {
	char *list_f = NULL;
	struct option longopts[] = {
		{ "list_file", optional_argument, NULL, 'l'},
		{     0,    0,    0,    0},
	};
	int c = -1;
	int remained_arg_idx = 1;
	while((c = getopt_long(argc, argv, ":l:", longopts, NULL)) != -1){
		switch (c){
		case 'l':
			list_f = optarg;
			break;
		}
		remained_arg_idx += 1; 
	}

	int resize_width = -1, resize_height = -1, resize_min_border = -1;
	std::vector<std::pair<std::string, std::string> > in_out;
	if (list_f == NULL) {
		if (argc != 5 && argc != 4) {
			printf("Usage: \n%s image_in image_out <resize_width resize_height | resize_min_border>\n", __FILE__);
			return 1;
		}
		if (argc == 5) {
			sscanf(argv[3], "%d", &resize_width);
			sscanf(argv[4], "%d", &resize_height);
		} else if (argc == 4) {
			sscanf(argv[3], "%d", &resize_min_border);
		}
		in_out.push_back( std::make_pair<std::string, std::string>(argv[1], argv[2]) );
	} else {
		if (remained_arg_idx + 2 != argc && remained_arg_idx + 1 != argc) {
			printf("Usage: \n%s --list_file=<list_file> <resize_width resize_height | resize_min_border>\n", __FILE__);
			return 1;
		}
		if (remained_arg_idx + 2 == argc) {
			sscanf(argv[remained_arg_idx + 0], "%d", &resize_width);
			sscanf(argv[remained_arg_idx + 1], "%d", &resize_height);
		} else if (remained_arg_idx + 1 == argc) {
			sscanf(argv[3], "%d", &resize_min_border);
		}

		std::ifstream flist(list_f);
		std::string inf, outf;
		while (flist >> inf >> outf) {
			in_out.push_back( std::make_pair(inf, outf) );
		}
	}

	if ((resize_width <= 0 || resize_height <= 0) && resize_min_border <= 0) {
		printf("[error] Invalid resize_width (%d) or resize_height (%d) or resize_min_border (%d)\n", resize_width, resize_height, resize_min_border);
		return 1;
	}

	int tot_converted = 0;
	for (int idx = 0; idx < in_out.size(); ++idx) {
		try {
			printf("[info] converting: %s to %s", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			cv::Mat img = cv::imread(in_out[idx].first.c_str());
			int new_width = resize_width, new_height = resize_height;
			if (resize_min_border != -1) {
				if (img.size().width < img.size().height) {
					new_width = resize_min_border;
					new_height = img.size().height * resize_min_border / img.size().width;
				} else {
					new_width = img.size().width   * resize_min_border / img.size().height;
					new_height = resize_min_border;
				}
			}
			printf(" (size: %d X %d)\n", new_width, new_height);
			cv::Mat out_img;
			cv::resize(img, out_img, cv::Size(new_width, new_height));
			cv::imwrite(in_out[idx].second.c_str(), out_img);
			printf("[info] converted: %s to %s\n", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			tot_converted += 1;
		} catch (std::exception e) {
			printf("[error] <%s> while converting file %s to %s\n", e.what(), in_out[idx].first.c_str(), in_out[idx].second.c_str());
		}
	}
	printf("[info] Converted %d / %lu images sucessfully\n", tot_converted, in_out.size());

	fclose(stdout);
	fclose(stderr);

	return 0;
}
	
