#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <fstream>
#include <string>
#include <getopt.h>
#include <utility>
#include <vector>
#include <algorithm>

int main(int argc, char *argv[]) {
	char *list_f = NULL;
  int nj = 1, nthj = 0;
	int resize_width = -1, resize_height = -1, resize_min_border = -1, resize_area = -1, crop_size = -1;
	struct option longopts[] = {
		{ "list_file", required_argument, NULL, 'l'},
    { "nj"       , required_argument, NULL, 'n'},
    { "nthj"     , required_argument, NULL, 'j'},
    { "width"    , required_argument, NULL, 'w'},
    { "height"   , required_argument, NULL, 'h'},
    { "min_dim"  , required_argument, NULL, 'd'},
    { "area"     , required_argument, NULL, 'a'},
    { "check_crop_size"     , required_argument, NULL, 'c'},
		{     0,    0,    0,    0},
	};
	int c = -1;
	int remained_arg_idx = 1;
	while((c = getopt_long(argc, argv, "l:n:j:w:h:d:a:c:", longopts, NULL)) != -1){
		switch (c){
		case 'l':
			list_f = optarg;
			break;
    case 'n':
      sscanf(optarg, "%d", &nj);
      break;
    case 'j':
      sscanf(optarg, "%d", &nthj);
      break;
    case 'w':
      sscanf(optarg, "%d", &resize_width);
      break;
    case 'h':
      sscanf(optarg, "%d", &resize_height);
      break;
    case 'd':
      sscanf(optarg, "%d", &resize_min_border);
      break;
    case 'a':
      sscanf(optarg, "%d", &resize_area);
      break;
    case 'c':
      sscanf(optarg, "%d", &crop_size);
      break;
		}
	}

	std::vector<std::pair<std::string, std::string> > in_out;
	if (list_f == NULL) {
		printf("Usage: \n%s -n<njobs> -j<nthjobs> -w<resize_width> -h<resize_height> -d<resize_min_dim> -a<resize_area>", __FILE__);
    return 1;
	}
  if (resize_area == -1 && resize_min_border == -1 && !(resize_width != -1 && resize_height != -1)) {
    printf("At lease one of resize_area, resize_min_border, resize_heightXresize_width is set.");
    return 2;
  }

  std::ifstream flist(list_f);
  std::string inf, outf;
  while (flist >> inf >> outf) {
    in_out.push_back( std::make_pair(inf, outf) );
  }

	int tot_converted = 0;
	for (int idx = 0; idx < in_out.size(); ++idx) {
    if (idx % nj != nthj) {
      continue;
    }
		try {
			printf("[info] converting: %s to %s", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			cv::Mat img = cv::imread(in_out[idx].first.c_str());
      int img_width = img.cols, img_height = img.rows;
      if (img_width <= 0 || img_height <= 0) {
        printf("[error] %s (size %d x %d) to %s\n", in_out[idx].first.c_str(), img_width, img_height, in_out[idx].second.c_str());
        continue;
      }
			int new_width = resize_width, new_height = resize_height;
      if (resize_area > 0) {
        float resize_ratio = sqrt( (resize_area + 0.0) / ( img_height * img_width ) );
        new_width = img_width * resize_ratio;
        new_height = img_height * resize_ratio;
        if (crop_size > 0 && new_height < crop_size || new_width < crop_size) {
          if (img_width < img_height) {
            new_width = crop_size;
            new_height = crop_size * img_height / img_width;
          } else {
            new_height = crop_size;
            new_width = crop_size * img_width / img_height;
          }
        }
      } else if (resize_min_border > 0) {
				if (img_width < img_height) {
					new_width = resize_min_border;
					new_height = img_height * resize_min_border / img_width;
				} else {
					new_width = img_width * resize_min_border / img_height;
					new_height = resize_min_border;
				}
			}
			printf(" (size: %d X %d)\n", new_width, new_height);
			cv::Mat out_img;
			cv::resize(img, out_img, cv::Size(new_width, new_height));
			cv::imwrite(in_out[idx].second.c_str(), out_img);
			printf("[info] converted: %s to %s\n", in_out[idx].first.c_str(), in_out[idx].second.c_str());
			tot_converted += 1;
		} catch (std::exception e) {
			printf("[error] <%s> while converting file %s to %s\n", e.what(), in_out[idx].first.c_str(), in_out[idx].second.c_str());
		}
	}
	printf("[info] Converted %d / %lu images sucessfully\n", tot_converted, in_out.size());

	fclose(stdout);
	fclose(stderr);

	return 0;
}
	
